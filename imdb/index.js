'use strict'
/// Allow this module to be reloaded by hotswap when changed
// Michael Howell(howellmichael.com) V1.02
module.change_code = 1;

var imdb = require('imdb-api');
var Alexa = require('alexa-app');

// Define an alexa-app
var app = new Alexa.app('imdb');

var UNKNOWNERROR = "Sorry, I didn't catch that.";
var REPROMPT = "Would you like anything else?";


var validator = function (title) {
    //logger("Title: " + title);
    if (title === null || title === "") {
        return "Sorry, I need a title for a movie.";
    }

    return true;
};

var logger = function (out) {
    console.log(out);
};

var say = function (res, prompt, open, card) {
    if (open === true) {
        if (card === undefined) {
            res.say(prompt).reprompt(REPROMPT).shouldEndSession(false).send();
        } else {
            res.say(prompt).reprompt(REPROMPT).shouldEndSession(false).card(card).send();
        }

    } else {
        res.say(prompt).send();
    }
};

var makeCard = function (data) {
    var card = {
        type: "Standard",
          //this is not required for type Simple OR Standard
        text:  "Length: " + data.runtime + "\n"+ "Rating: " +data.rated + "\n" + "Plot: " +data.plot
    };

    if(data.title !== undefined && data.title !== "" && data.title !== "N/A"){
        card.title = data.title;
    }


    if(data.poster !== undefined && data.poster !== "" && data.poster !== "N/A"){
        card.image = {                //image is optional
            largeImageUrl: data.poster
        };
    }
    logger(card);
    return card;
};

app.launch(function (req, res) {

    var prompt = "Welcome to Dev. You can ask me about release dates, ratings and length of movies. Try asking when was gladiator released.";
    say(res, prompt, true);
});

app.intent('dateIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    ,
    "utterances": ["when was {TITLE} released", "when did {TITLE} come out", "what date was {TITLE} released", "what date did {TITLE} come out"]
}, function (req, res) {
    var title = req.slot('TITLE');
    logger("Title: " + title);

    imdb.get(title).then(function (data) {
        say(res, data.title + " was released in " + data.year, true, makeCard(data));
    }).catch(function (error) {
        logger("Error");
        logger(error);
        say(res, UNKNOWNERROR, true);
    });
    return false;
});

app.intent('ratedIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    , "utterances": ["what was {TITLE} rated", "is {TITLE} good", "if {TITLE} is good"]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {

        imdb.get(title).then(function (data) {
            say(res, data.title + " was rated a " + data.rated + " from " + data.votes + " and has score of " + data.metascore + " on meta critic.", true, makeCard(data));
        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});

app.intent('runtimeIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    , "utterances": ["how long is {TITLE}", "what length is {TITLE}"]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {

        imdb.get(title).then(function (data) {
            say(res, data.title + " is " + data.runtime + " long", true, makeCard(data));
        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});

app.intent('plotIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    ,
    "utterances": ["tell me about {TITLE}", "what was the plot of {TITLE}", "what was {TITLE} about", "tell me the plot of {TITLE}"]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {
        imdb.get(title).then(function (data) {
            say(res, data.title + " is about " + data.plot, true, makeCard(data));
        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});


app.intent('directorIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    , "utterances": ["who directed {TITLE}", "who made {TITLE}"]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {
        imdb.get(title).then(function (data) {
            say(res, data.title + " was directed by " + data.director, true, makeCard(data));
        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});

app.intent('actorsIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    ,
    "utterances": ["who was in {TITLE}", "tell me who was in {TITLE}", "tell me actors in {TITLE}", "actors in {TITLE}"]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {
        imdb.get(title).then(function (data) {
            say(res, data.actors + " stared in " + data.title, true, makeCard(data));
        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});

app.intent('writerIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    , "utterances": ["who wrote {TITLE}", "who penned {TITLE}", "who created {TITLE}"]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {
        imdb.get(title).then(function (data) {
            say(res, data.title + " was written by " + data.writer, true, makeCard(data));
        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});

app.intent('languagesIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    , "utterances": ["what language was {TITLE} in", "what languages was {TITLE} in",]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {
        imdb.get(title).then(function (data) {
            var ans = "";
            for (var i = 0; i < data.languages.length; i++) {
                ans += data.languages[i];
            }

            if (ans !== "") {
                say(res, data.title + " was in " + ans, true);
            } else {
                say(res, "sorry, I couldn't find and language data on " + data.title, true, makeCard(data));
            }

        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});

app.intent('awardsIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    ,
    "utterances": ["what did {TITLE} win", "what has {TITLE} won", "what has {TITLE} won", "what awards has {TITLE} won", "awards for {TITLE}"]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {
        imdb.get(title).then(function (data) {
            say(res, data.title + " won " + data.awards, true, makeCard(data));
        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});

app.intent('posterIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    , "utterances": ["what does {TITLE} poster look like"]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {
        imdb.get(title).then(function (data) {
            say(res, "Sorry I can't send things yet.", true, makeCard(data));
        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});

app.intent('allIntent', {
    "slots": {
        'TITLE': 'STRING'
    }
    , "utterances": ["tell me all about {TITLE}", "{TITLE}", "tell me everything about {TITLE}"]
}, function (req, res) {
    var title = req.slot('TITLE');
    var validation = validator(title);
    if (validation !== true) {
        say(res, validation, true);
    } else {
        imdb.get(title).then(function (data) {
            makeCard(res, data);
            say(res, data.title + " was released in " + data.year + ". It had a " + data.rating + " rating." + data.title + " is about " + data.plot, true, makeCard(data));
        }).catch(function (error) {
            logger("Error");
            logger(error);
            say(res, UNKNOWNERROR, true);
        });
    }
    return false;
});

app.intent('noIntent', {
    "slots": { }
    , "utterances": ["no", "nope", "nahh"]
}, function (req, res) {

    res.say("Thankyou").shouldEndSession(true);
});

app.intent('yesIntent', {
    "slots": { }
    , "utterances": ["yeah", "yes", "yup"]
}, function (req, res) {
    say(res, REPROMPT, true);
});

module.exports = app;
