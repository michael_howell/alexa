# README #

This is a project for the SI Hackathon as a test app for Alexa. 

### What is this repository for? ###

* This repo will be a sandbox for Alexa development.
* Version 0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
## Dev ##
* Install Node
* Pull Alexa Server code 
```
#!
cd <dir>
git clone https://github.com/matt-kruse/alexa-app-server.git
```

* Get dependences

```
#!
cd alexa-app-server
npm install
```

* Get this code 

```
#!
cd examples\apps

```
Clean this directory then pull

```
#!
git clone https://michael_howell@bitbucket.org/michael_howell/alexa.git
```
* Get dependences

```
#!
cd IMDB
npm install
```
* Change the applicationId in package.json to your apps.

```
#!JSON
{
  "name": "Movies",
  "version": "1.0.0",
  "description": "An Alexa skill for Movie infromation from IMDB.",
  "main": "index.js",
  "author": "Michael Howell <https://bitbucket.org/michael_howell/> (http://howellmichael.com/)",
  "license": "ISC",
  "alexa": {
    "applicationId": "amzn1.ask.skill.XXXXXXXX-XXXX-XXX-XXXX-XXXXXXXXXXX"
  },
  "dependencies": {
    "alexa-app": "^2.3.4",
    "imdb-api": "^2.2.0"
  }
}
```

* Launch the program using Node

```
#!
cd ../..
node server
```

* Goto http://localhost:8083/alexa/imdb in browser to test.

##Deploy##
#Manual#
* Deploy an AWS lambda Alexa skill in Ireland. You can sign in as SIDev01@gmail.com.
https://eu-west-1.console.aws.amazon.com/lambda/home?region=eu-west-1#/functions?display=list
* Create an Alexa app. 
https://developer.amazon.com/
#Tool#
I have used an AWS tool to make this easier.

```
#!

npm install -g aws-lambda
```
Edit the .lambda file with your credetials.

Edit the deploy shell script if needed 
Run the shell script. 


```
#!javascript


```




### Contribution guidelines ###
None

### Who do I talk to? ###

* Michael Howell